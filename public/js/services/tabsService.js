"use strict";

var app = angular.module("boxTabsApp");

app.factory('tabsService', ['$http', function($http) {
    return {
        getJsonData : function() {
            return $http.jsonp('http://rexxars.com/playground/testfeed/?callback=JSON_CALLBACK');
        },

        getRssData : function() {
            return  $http.jsonp("//ajax.googleapis.com/ajax/services/feed/load?callback=JSON_CALLBACK", { params: { "v": "1.0", "q": "http://www.vg.no/rss/feed/forsiden/?frontId=1", num: 50} })
        },

        getFileData : function() {
            return  $http.get('files');
        }
    }
}]);