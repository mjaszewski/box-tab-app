"use strict";

var app = angular.module("boxTabsApp");

app.controller("fileTabController", ['$scope', 'tabsService', function ($scope, tabsService) {

    function successCallback(response){
        $scope.hostData = response.data.hosts;
        $scope.fileData = response.data.files;
        $scope.isLoading = false;

        $scope.orderByFieldFiles = 'requests';
        $scope.orderByFieldHosts = 'traffic';

        $scope.reverseSortFiles = true;
        $scope.reverseSortHosts = true;
    }

    function errorCallback(response){
        $scope.error = true;
        $scope.data = response.data;
    }

    $scope.init = function () {
        $scope.error = false;
        $scope.isLoading = true;
        tabsService.getFileData().then(successCallback, errorCallback);
    };
}]);