"use strict";

var app = angular.module("boxTabsApp");

app.controller("jsonTabController", ['$scope', 'tabsService', function ($scope, tabsService) {

    var months = ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober",
        "november", "desember"];
    $scope.dataSize = 0;

    function convertDate() {
        angular.forEach($scope.data, function (value) {
            var date = value.date.split(" "),
                time = value.time.split(":"),
                convertDate;

            date[1] = months.indexOf(date[1].toLowerCase());
            convertDate = new Date(date[2], date[1], date[0], time[0], time[1], 0);
            value.convertDate = convertDate;
            $scope.dataSize++;
        });
        $scope.isLoading = false
    }

    function successCallback(response) {
        $scope.data = response.data;
        if ($scope.data) {
            convertDate()
        }

        $scope.isLoading = false
    }

    function errorCallback(response) {
        $scope.error = true;
        $scope.data = response.data;
    }

    $scope.init = function () {
        $scope.error = false;
        $scope.isLoading = true;
        tabsService.getJsonData().then(successCallback, errorCallback);
    };
}]);