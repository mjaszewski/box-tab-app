"use strict";

var app = angular.module("boxTabsApp");

app.controller("rssTabController", ['$scope', 'tabsService', function ($scope, tabsService) {

    $scope.dataSize = 0;

    function successCallback(response) {
        $scope.data = response.data.responseData.feed.entries;
        if (response.data.responseData.feed.entries) {
            angular.forEach($scope.data, function (value) {
                value.pubDate = Date.parse(new Date(value.publishedDate));
                $scope.dataSize++;
            });
        }
        $scope.isLoading = false;
    }

    function errorCallback() {
        $scope.error = true;
        $scope.data = response.data;
    }

    $scope.init = function () {
        $scope.error = false;
        $scope.isLoading = true;
        tabsService.getRssData().then(successCallback, errorCallback);
    };
}]);