"use strict";

var app = angular.module("boxTabsApp");

app.controller("mainController", ['$scope', '$route', function ($scope, $route) {
    $scope.route = $route;
}]);