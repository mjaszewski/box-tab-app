"use strict";

var app = angular.module("boxTabsApp", ["ngRoute", "ngAnimate"]);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'views/partials/fileTab.html',
            controller: 'fileTabController',
            activeTab: 'files'
        })

        .when('/rss', {
            templateUrl: 'views/partials/rssTab.html',
            controller: 'rssTabController',
            activeTab: 'rss'
        })

        .when('/json', {
            templateUrl: 'views/partials/jsonTab.html',
            controller: 'jsonTabController',
            activeTab: 'json'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);

}]);
