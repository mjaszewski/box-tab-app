"use strict";

var path = require('path'),
    readline = require('readline'),
    fs = require('fs'),
    _ = require('lodash');

module.exports = function (app) {
    app.get('/files', function (req, res) {
        var files = [],
            hostnames = [],
            hostTraffic = {},
            sortedHostTraffic,
            sortedFileRequests,
            sortedHostTrafficObjects = [],
            sortedFileRequestsObjects = [],
            countedFiles,
            lineToArray,
            hostname,
            traffic,
            file,
            rl = readline.createInterface({
                input: fs.createReadStream('varnish.log')
            });

        rl.on('line', function (line) {
            return (splitLines(line));
        });

        function splitLines(line) {
            lineToArray = line.split(" ");
            hostname = lineToArray[0];
            traffic = parseInt(lineToArray[9]);
            file = lineToArray[6];

            hostnames.push([hostname, traffic]);
            files.push(file);
        }

        function sortObject(sorted, toSort, firstProp, secondProp) {
            sorted = _.toPairs(toSort);
            sorted.sort(function (a, b) {
                return b[1] - a[1]
            });
            sorted = _.slice(sorted, 0, 5);

            var sortedArraySize = sorted.length,
                sortedObjects = [];


            for (var j = 0; sortedArraySize > j; j++) {
                var obj = {};
                obj[firstProp] = sorted[j][0];
                obj[secondProp] = sorted[j][1];
                sortedObjects.push(obj)
            }
            return sortedObjects
        }

        function getHostTraffic() {
            var size = hostnames.length,
                hostname,
                traffic;

            for (var i = 0; size > i; i++) {
                hostname = hostnames[i][0];
                traffic = hostnames[i][1];

                if (typeof hostTraffic[hostname] === 'undefined') {
                    hostTraffic[hostname] = traffic;
                } else {
                    hostTraffic[hostname] += traffic;
                }
            }
            return sortObject(sortedHostTraffic, hostTraffic, 'host', 'traffic');
        }

        function getFileRequests() {
            countedFiles = _.countBy(files, _.identity);
            return sortObject(sortedFileRequests, countedFiles, 'filename', 'requests');
        }

        rl.on('close', function () {
            sortedHostTrafficObjects = getHostTraffic();
            sortedFileRequestsObjects = getFileRequests();
            res.send({hosts: sortedHostTrafficObjects, files: sortedFileRequestsObjects});
        });
    });

    app.get('*', function (req, res) {
        res.sendFile(path.resolve('./public/views/index.html'));
    });
};