"use strict";

var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));
require('./app/routes')(app);

app.listen(port);
console.log('Server is listening on port ' + port);
module.exports = app;