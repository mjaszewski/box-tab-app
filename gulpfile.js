"use strict";

var gulp = require("gulp"),
    sass = require("gulp-sass"),
    sourcemaps = require('gulp-sourcemaps'),
    useref = require('gulp-useref'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    config = {
        public: './public/',
        distPublic : './dist/public/'
    };

gulp.task('html', function () {
    return gulp.src(config.public+'views/index.html')
        .pipe(useref())
        .pipe(gulp.dest(config.distPublic+'views/'));
});

gulp.task('copy', function () {
    return gulp.src(config.public+'views/partials/*.html')
        .pipe(gulp.dest(config.distPublic+'views/partials/'));
});

gulp.task('css', ['html'] , function(){
    gulp.src(config.distPublic+'css/*.css')
        .pipe(minifyCss())
        .pipe(gulp.dest(config.distPublic+'css/'))
});

gulp.task('js', ['html'], function(){
    gulp.src(config.distPublic+'js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(config.distPublic+'js/'))
});


gulp.task('styles', function () {
    return gulp.src(config.public+'sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.public+'css'));
});

gulp.task('watch', ['styles'], function () {
    gulp.watch(config.public+'sass/*.scss', ['styles']);
});

gulp.task('dist', ['html', 'copy', 'css', 'js']);